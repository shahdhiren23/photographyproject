from django.contrib.auth.models import User
from django.shortcuts import render
from django.views.generic import *
from .forms import *
from .models import *


class ClientMixin(object):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['persinfo'] = PersonalInformation.objects.first()
        context['aboutinfo'] = About.objects.all()
        context['allcategories'] = Category.objects.all()
        return context


class ClientHomeView(ClientMixin, TemplateView):
    template_name = "clienttemplates/clienthome.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['allsliders'] = Slider.objects.all()
        context['awards'] = Award.objects.all()
        context['alltestimonials'] = Testimonial.objects.all()
        context['page'] = "home"
        return context


class ClientCategoryDetailView(ClientMixin, DetailView):
    template_name = "clienttemplates/clientcategorydetail.html"
    model = Category
    context_object_name = "categoryobject"


class AboutView(ClientMixin, TemplateView):
    template_name = "clienttemplates/about.html"


class ContactView(ClientMixin, TemplateView):
    template_name = "clienttemplates/contact.html"


class ClientProjectListView(ClientMixin, ListView):
    template_name = "clienttemplates/clientprojectlist.html"
    queryset = Project.objects.all()
    context_object_name = "projects"

    def get_queryset(self):
        status = self.request.GET['status']
        print(status, "********************************")
        return Project.objects.filter(status=status)


class ClientTearSheetView(ClientMixin, ListView):
    template_name = "clienttemplates/clienttearsheet.html"
    queryset = Tearsheet.objects.all()
    context_object_name = "alltearsheet"
