from django.urls import path
from .views import *

app_name = "photographyapp"

urlpatterns = [
    path("", ClientHomeView.as_view(), name="clienthome"),
    path("tearsheet/", ClientTearSheetView.as_view(), name="clienttearsheet"),
    path("about", AboutView.as_view(), name="about"),
    path("contact", ContactView.as_view(), name="contact"),
    path("photo/<int:pk>/", ClientCategoryDetailView.as_view(),
         name="clientcategorydetail"),
    path('projects/', ClientProjectListView.as_view(),
         name="clientprojectlist"),



]
