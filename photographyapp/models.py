from django.db import models


class PersonalInformation(models.Model):
    name = models.CharField(max_length=200)
    logo = models.ImageField(upload_to="personalbio")
    nickname = models.CharField(max_length=200, null=True, blank=True)
    image = models.ImageField(upload_to="personalbio")
    email = models.EmailField()
    mobile = models.CharField(max_length=50)
    address = models.CharField(max_length=500)
    education = models.CharField(max_length=200)
    about = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.name


class Slider(models.Model):
    title = models.CharField(max_length=200)
    caption = models.CharField(max_length=200)
    image = models.ImageField(upload_to="sliders")

    def __str__(self):
        return self.title


class Award(models.Model):
    title = models.CharField(max_length=200)
    image = models.ImageField(upload_to="awards")
    date = models.DateField()
    description = models.TextField()
    provided_by = models.CharField(max_length=200)

    def __str__(self):
        return self.title


class Category(models.Model):
    title = models.CharField(max_length=200)
    image = models.ImageField(upload_to="category", null=True, blank=True)

    def __str__(self):
        return self.title


class Photo(models.Model):
    title = models.CharField(max_length=200)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    image = models.ImageField(upload_to="photos")
    caption = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.title


class Video(models.Model):
    title = models.CharField(max_length=200)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    link = models.CharField(max_length=200)
    description = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.title


STATUS = (
    ('ongoing', 'On going project'),
    ('completed', 'Completed project'),
)


class Project(models.Model):
    title = models.CharField(max_length=200)
    description = models.TextField()
    status = models.CharField(max_length=200, choices=STATUS)
    completion_pct = models.PositiveIntegerField(null=True, blank=True)

    def __str__(self):
        return self.title


class Message(models.Model):
    sender = models.CharField(max_length=200)
    mobile = models.CharField(max_length=200)
    email = models.EmailField(null=True, blank=True)
    message = models.TextField()
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.sender


class Testimonial(models.Model):
    name = models.CharField(max_length=200)
    image = models.ImageField(upload_to="testimonial")
    sayings = models.CharField(max_length=600)
    current_engagement = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class Tearsheet(models.Model):
    image = models.ImageField(upload_to="category")

    def __str__(self):
        return str(self.id)


class About(models.Model):
    name = models.CharField(max_length=200)
    image = models.ImageField(upload_to="image")
    description = models.TextField()

    def __str__(self):
        return self.name
