from django.contrib import admin
from .models import *


admin.site.register([PersonalInformation, Award, Category,
                     Photo, Video, Project, Message, Testimonial,Slider,Tearsheet,About])
