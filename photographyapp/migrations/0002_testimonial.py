# Generated by Django 2.2.3 on 2019-08-07 11:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('photographyapp', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Testimonial',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
                ('image', models.ImageField(upload_to='testimonial')),
                ('sayings', models.CharField(max_length=600)),
                ('current_engagement', models.CharField(max_length=200)),
            ],
        ),
    ]
