# Generated by Django 2.2.4 on 2019-08-14 12:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('photographyapp', '0006_auto_20190814_1709'),
    ]

    operations = [
        migrations.CreateModel(
            name='About',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
                ('image', models.ImageField(upload_to='image')),
                ('description', models.TextField()),
            ],
        ),
    ]
